#!/bin/sh
# autopkgtest check: Build and run a program against glib, to verify that the
# headers and pkg-config file are installed correctly
# (C) 2012 Jose Luis Rivero
# Author: Jose Luis Rivero <jrivero@osrfoundation.org>

set -e

WORKDIR=$(mktemp -d)
#trap "rm -rf $WORKDIR" 0 INT QUIT ABRT PIPE TERM
echo $WORKDIR
cd $WORKDIR
cat <<EOF > sample.em
#! \$Id: sample.em 5359 2014-01-23 00:33:57Z max $ \$Date: 2014-01-22 16:33:57 -0800 (Wed, 22 Jan 2014) $
@# Bangpaths are handled properly (the above line will not appear).
#! This line however will appear (not the first line in the script).
@# This is a comment.  This should not appear in the processed output.
This is text.  It should appear in the processed output. 
This is a literal at sign: @@.
This is a line continuation; @
this will appear on the same line.
Note that it will actually eat any white@ space (one word).
@{
# The em.py script has to be somewhere in sys.path!
import em
}@

@# Escape codes.
This will appear on one line.@\nThis will appear on a separate line.
This is separated by a tab:@\tSee?
These are uppercase As (presuming ASCII): A, @\q1001, @\o101, @\d065, @\x41.
@{
import sys
# This is just a normal Python comment.
print("This is more text.")
}@
@# Note the @{ ... }@ convention to suppress the newline following the }.
@# Also note that comments are completely tossed: This is not expanded: @(x).

@# The basics.
@{
import sys, math
x = 4
s = 'alpha'
word = "book"
l = [3, 2, 1]
def square(n):
    return n**2
friends = ['Albert', 'Betty', 'Charles', 'Donald']
class Container(object):

    def __init__(self, value):
        self.value = value

    def square(self):
        return square(self.value)
c = Container(3)
}@
The basics: The square of @(x) is @(x**2), or @(square(x)).
Internal whitespace is fine: @( x ) squared is @( square(x) ).
Statements: @{sys.stdout.write("%d**2 = %d" % (x, square(x)))}.
Whitespace too: @{ sys.stdout.write("%d**2 = %d (still)" % (x, square(x))) }.
@{
print("But only on single-line statement expansions.")
if 1:
    print("Internal whitespace on multi-line statements is significant.")
for i in range(2):
    print("Normal Python indentation rules must be followed here.")
}@
Simple expressions: x is @x, l is @l, s is "@s," and @x squared is @square(x).
Literals too: x is @x, but would be written @@x.
Trailing dots are ignored: The value of x is @x.
Quotes outside of expansions are also ignored: This is quoted: "x is @x."
@# Whitespace is important in simple expressions.
Array subscription: The first element of l is @l[0].
But this is not: The first element of l is not @l [0].
That was equivalent to: @(l) and then [0], not @l[0].
But whitespace can go inside the brackets: @l[0] is @l[ 0 ].
Same with functions: @square(x) is @square( x ).
The same applies to the other forms.
Involved: The contained value is @c.value.
More involved: The square of the contained value is @c.square().
Following expressions: Pluralize "@word" as "@(word)s," or maybe "@word@ s."
By default str is used (@s), but you can use repr if you want (@\`s\`).
Conditional expressions: @(x ? "x is true" ! "x is false").
Pluralization: How many words? @x word@(x != 1 ? 's').
Protected expressions: @(foo $ "foo is not defined").
Also here, whitespace isn't important: @(bar$"bar isn't defined either").
The math module has @(math ? "been imported" $ "not been imported").
The re module has @(re ? "been imported" $ "not been imported").
Division by zero is @(x/0 $ "illegal").
To swallow errors, use None: @(buh $ None) [two spaces].
This is self-expanding: @:2 + 2:(this will get replaced with 4):
You can expand multiple times: @
@empy.expand("@empy.expand('@:2 + 2:hugalugahglughalug:')")
EOF

cat <<EOF2 > sample.bench
#! This line however will appear (not the first line in the script).
This is text.  It should appear in the processed output. 
This is a literal at sign: @.
This is a line continuation; this will appear on the same line.
Note that it will actually eat any whitespace (one word).

This will appear on one line.
This will appear on a separate line.
This is separated by a tab:	See?
These are uppercase As (presuming ASCII): A, A, A, A, A.
This is more text.

The basics: The square of 4 is 16, or 16.
Internal whitespace is fine: 4 squared is 16.
Statements: 4**2 = 16.
Whitespace too: 4**2 = 16 (still).
But only on single-line statement expansions.
Internal whitespace on multi-line statements is significant.
Normal Python indentation rules must be followed here.
Normal Python indentation rules must be followed here.
Simple expressions: x is 4, l is [3, 2, 1], s is "alpha," and 4 squared is 16.
Literals too: x is 4, but would be written @x.
Trailing dots are ignored: The value of x is 4.
Quotes outside of expansions are also ignored: This is quoted: "x is 4."
Array subscription: The first element of l is 3.
But this is not: The first element of l is not [3, 2, 1] [0].
That was equivalent to: [3, 2, 1] and then [0], not 3.
But whitespace can go inside the brackets: 3 is 3.
Same with functions: 16 is 16.
The same applies to the other forms.
Involved: The contained value is 3.
More involved: The square of the contained value is 9.
Following expressions: Pluralize "book" as "books," or maybe "books."
By default str is used (alpha), but you can use repr if you want ('alpha').
Conditional expressions: x is true.
Pluralization: How many words? 4 words.
Protected expressions: foo is not defined.
Also here, whitespace isn't important: bar isn't defined either.
The math module has been imported.
The re module has not been imported.
Division by zero is illegal.
To swallow errors, use None:  [two spaces].
This is self-expanding: @:2 + 2:4:
You can expand multiple times: @:2 + 2:4:
EOF2

if empy3 sample.em | diff sample.bench -; then
  echo "empy call ok"
else
  exit 1
fi
